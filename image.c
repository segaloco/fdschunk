/*
 * image - Famicom Disk System Image routines
 *
 * Copyright 2023 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "superblock.h"
#include "file.h"

#include "image.h"

#define	FDS_IMAGE_SIZE			65500

fds_image_t *fds_image_create(fds_image_t *image)
{
	fds_superblock_create(&image->superblock);
	image->files = NULL;

	return image;
}

int fds_image_read(fds_image_t *image, FILE *file, int cflag)
{
	int i, size;

	size = fds_superblock_read(&image->superblock, file, cflag);

	image->files = malloc(image->superblock.file_count * sizeof (*image->files));
	for (i = 0; i < image->superblock.file_count; i++)
		size += fds_file_read(&image->files[i], file, cflag);

	return feof(file) ? -1 : size;
}

int fds_image_validate(const fds_image_t *image)
{
	int i;

	if (!fds_superblock_validate(&image->superblock))
		return 0;

	for (i = 0; i < image->superblock.file_count; i++)
		if (!fds_file_validate(&image->files[i]))
			return 0;

	return 1;
}

int fds_image_write(const fds_image_t *image, FILE *file, int cflag)
{
	int i, size, size_diff;

	size = fds_superblock_write(&image->superblock, file, cflag);

	for (i = 0; i < image->superblock.file_count; i++)
		size += fds_file_write(&image->files[i], file, cflag);

	size_diff = FDS_IMAGE_SIZE - size;

	if (size_diff)
		for (i = 0; i < size_diff; i++)
			putc('\0', file);

	return ferror(file) ? -1 : FDS_IMAGE_SIZE;
}

size_t fds_image_fprintf(FILE *file, const fds_image_t *image)
{
	int i;
	size_t size;

	size = fds_superblock_fprintf(file, &image->superblock);

	for (i = 0; i < image->superblock.file_count; i++)
		size += fds_file_fprintf(file, &image->files[i]);

	return ferror(file) ? -1 : size;
}

void fds_image_free(fds_image_t *image)
{
	int i;

	for (i = 0; i < image->superblock.file_count; i++)
		fds_file_free(&image->files[i]);

	if (image->files != NULL) {
		free(image->files);
		image->files = NULL;
	}

	image->superblock.file_count = 0;
}
