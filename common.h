/*
 * common - libfdsimage commons
 *
 * Copyright 2023 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
#ifndef	COMMON_H
#define	COMMON_H

#ifdef	__cplusplus
extern "C" {
#endif	/* __cplusplus */

#include <stdio.h>
#include <limits.h>

#define	UNKNOWN_STRING	"Unknown"
#define	CRC_SIZE	2

#define	getw_m(word, file) {			\
	word = getc(file);			\
	word |= (getc(file) << CHAR_BIT);	\
}

#define putw_m(word, file) {                    \
        putc(((word) & UCHAR_MAX), (file));     \
        putc(((word) >> CHAR_BIT), (file));	\
}

#define fprintw_m(file, word) {				\
	fprintf(file, "%.2x", ((word) >> CHAR_BIT));	\
	fprintf(file, "%.2x", ((word) & UCHAR_MAX));	\
}

#ifdef	__cplusplus
}
#endif	/* __cplusplus */

#endif	/* COMMON_H */
