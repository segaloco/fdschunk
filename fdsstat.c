#include <stdio.h>

#include "./image.h"

int main(void)
{
	fds_image_t image;

	fds_image_read(&image, stdin, 0);
	fds_image_fprintf(stdout, &image);
	fds_image_free(&image);

	return 0;
}