BIN		=	fdschunk

FDSSTAT		=	fdsstat

MAIN		=	main.o

FDSSTAT_OBJS	=	fdsstat.o

OBJS		=	$(MAIN)

FDSLIB		=	libfdsimage.a

FDSLIB_OBJS	=	bcd_date.o \
			superblock.o \
			file.o \
			image.o

$(BIN): $(OBJS) $(FDSLIB)
	$(CC) $(LDFLAGS) -o $(BIN) $(OBJS) $(FDSLIB)

$(FDSSTAT): $(FDSSTAT_OBJS)
	$(CC) $(LDFLAGS) -o $(FDSSTAT) $(FDSSTAT_OBJS) $(FDSLIB)

$(FDSLIB): $(FDSLIB_OBJS)
	$(AR) -cr $(FDSLIB) $(FDSLIB_OBJS)

clean:
	rm -rf $(BIN) $(FDSLIB) $(OBJS) $(FDSLIB_OBJS)
