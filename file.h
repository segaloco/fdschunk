/*
 * file - Famicom Disk System Image file routines
 *
 * Copyright 2023 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
#ifndef	FDS_FILE_H
#define	FDS_FILE_H

#ifdef	__cplusplus
extern "C" {
#endif	/* __cplusplus */

#include <stddef.h>
#include <stdio.h>

#define	FDS_FILENAME_MAX	8

typedef enum fds_file_type {
	FDS_FILE_TYPE_PRG,
	FDS_FILE_TYPE_CHR,
	FDS_FILE_TYPE_BG
} fds_file_type_t;

typedef struct fds_file {
	unsigned char start_marker;
	unsigned char file_number;
	unsigned char file_code;
	char file_name[FDS_FILENAME_MAX];
	unsigned short file_dest;
	unsigned short file_size;
	fds_file_type_t file_type;
	unsigned short crc0;
	unsigned char payload_marker;
	unsigned char *file_data;
	unsigned short crc1;
} fds_file_t;

extern fds_file_t *fds_file_create(fds_file_t *file);
extern int fds_file_read(fds_file_t *file, FILE *image, int cflag);
extern int fds_file_validate(const fds_file_t *file);
extern int fds_file_write(const fds_file_t *file, FILE *image, int cflag);
extern size_t fds_file_fprintf(FILE *out, const fds_file_t *file);
extern void fds_file_free(fds_file_t *file);

#ifdef	__cplusplus
}
#endif	/* __cplusplus */

#endif	/* FILE_H */
