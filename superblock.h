/*
 * superblock - Famicom Disk System Image superblock routines
 *
 * Copyright 2023 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
#ifndef	FDS_SUPERBLOCK_H
#define	FDS_SUPERBLOCK_H

#ifdef	__cplusplus
extern "C" {
#endif	/* __cplusplus */

#include <stddef.h>
#include <stdio.h>

#include "./bcd_date.h"

#define	FDS_SECURITY_STRING	"*NINTENDO-HVC*"

typedef enum fds_licensee_code {
	FDS_LICENSEE_CODE_NULL,
	FDS_LICENSEE_CODE_NINTENDO
} fds_licensee_code_t;

typedef enum fds_game_type {
	FDS_GAME_TYPE_NORMAL  = ' ',
	FDS_GAME_TYPE_EVENT   = 'E',
	FDS_GAME_TYPE_NETWORK = 'J',
	FDS_GAME_TYPE_REDUCED = 'R'
} fds_game_type_t;

typedef enum fds_disk_side {
	FDS_DISK_SIDE_A,
	FDS_DISK_SIDE_B
} fds_disk_side_t;

typedef enum fds_disk_type {
	FDS_DISK_TYPE_NORMAL,
	FDS_DISK_TYPE_FMC
} fds_disk_type_t;

typedef enum fds_disk_region {
	FDS_DISK_REGION_NULL,
	FDS_DISK_REGION_JAPAN	= 0x49
} fds_disk_region_t;

typedef enum fds_disk_color {
	FDS_DISK_COLOR_YELLOW,
	FDS_DISK_COLOR_PROTO = 0xFE,
	FDS_DISK_COLOR_BLUE
} fds_disk_color_t;

typedef struct fds_superblock {
	unsigned char start_marker;
	char security_string[sizeof (FDS_SECURITY_STRING)-1];
	fds_licensee_code_t licensee_code;
	char program_title[3];
	fds_game_type_t game_type;
	unsigned char game_version;
	fds_disk_side_t disk_side;
	unsigned char disk_number;
	fds_disk_type_t disk_type;
	unsigned char pad0;
	unsigned char start_file_max;
	unsigned char pad1[5];
	fds_bcd_date_t date_created;
	fds_disk_region_t disk_region;
	unsigned char unk0;
	unsigned char unk1;
	unsigned char unk2[2];
	unsigned char unk3[5];
	fds_bcd_date_t date_modified;
	unsigned char unk4;
	unsigned char unk5;
	unsigned short disk_writer;
	unsigned char unk6;
	unsigned char disk_write_count;
	fds_disk_side_t disk_side_real;
	fds_disk_color_t disk_color;
	unsigned char disk_version;
	unsigned short crc0;
	unsigned char file_count_marker;
	unsigned char file_count;
	unsigned short crc1;
} fds_superblock_t;

extern fds_superblock_t *fds_superblock_create(fds_superblock_t *superblock);
extern int fds_superblock_read(fds_superblock_t *superblock, FILE *image, int cflag);
extern int fds_superblock_validate(const fds_superblock_t *superblock);
extern int fds_superblock_write(const fds_superblock_t *superblock, FILE *image, int cflag);
extern size_t fds_superblock_fprintf(FILE *file, const fds_superblock_t *superblock);

#ifdef	__cplusplus
}
#endif	/* __cplusplus */

#endif	/* FDS_SUPERBLOCK_H */
