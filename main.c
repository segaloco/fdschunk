/*
 * fdschunk - break Famicom Disk System image into pieces
 *
 * Copyright 2023 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "image.h"

#define	BUFFER_SIZE	(FDS_FILENAME_MAX*3)+1

char *fds_name_norm(char *name, int entropy)
{
	int i, j;
	char buffer[BUFFER_SIZE] = { '\0' };

	if (name[0] != '\xFF') {
		for (i = 0, j = 0; name[i] != '\0'; i++) {
			if (name[i] == '-') {
				buffer[j++] = '_';
			} else if (name[i] == '.') {
				buffer[j++] = '.';
			} else if (isalpha(name[i])) {
				buffer[j++] = tolower(name[i]);
			} else if (isdigit(name[i])) {
				buffer[j++] = name[i];
			} else {
				buffer[j++] = 'x';
				sprintf(&buffer[j], "%.2x", name[i]);
				j += 2;
			}
		}
		buffer[j] = '\0';
	} else {
		sprintf(buffer, "file%d", entropy);
	}

	strcpy(name, buffer);

	return name;
}

int main(void)
{
	int i, j;
	FILE *outfile;
	char ext[] = ".bin";
	char namebuf[BUFFER_SIZE] = { '\0' };
	char binbuf[BUFFER_SIZE+4] = { '\0' };

	int cflag = 0;
	fds_image_t image;

	fds_image_read(&image, stdin, cflag);

	for (i = 0; i < image.superblock.file_count; i++) {
		strncpy(namebuf, image.files[i].file_name, sizeof (image.files[i].file_name));
		namebuf[sizeof (image.files[i].file_name)] = '\0';
		fds_name_norm(namebuf, i);
		sprintf(binbuf, "%s%s", namebuf, ext);

		outfile = fopen(binbuf, "wb");

		fwrite(image.files[i].file_data, sizeof (*image.files[i].file_data), image.files[i].file_size, outfile);

		fclose(outfile);
	}

	fds_image_free(&image);

	return 0;
}