/*
 * image - Famicom Disk System Image routines
 *
 * Copyright 2023 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
#ifndef	FDS_IMAGE_H
#define	FDS_IMAGE_H

#ifdef	__cplusplus
extern "C" {
#endif	/* __cplusplus */

#include <stddef.h>
#include <stdio.h>

#include "./superblock.h"
#include "./file.h"

typedef struct fds_image {
	fds_superblock_t superblock;
	fds_file_t *files;
} fds_image_t;

extern fds_image_t *fds_image_create(fds_image_t *image);
extern int fds_image_read(fds_image_t *image, FILE *file, int cflag);
extern int fds_image_validate(const fds_image_t *image);
extern int fds_image_write(const fds_image_t *image, FILE *file, int cflag);
extern size_t fds_image_fprintf(FILE *file, const fds_image_t *image);
extern void fds_image_free(fds_image_t *image);

#ifdef	__cplusplus
}
#endif	/* __cplusplus */

#endif	/* IMAGE_H */
