/*
 * superblock - Famicom Disk System Image superblock routines
 *
 * Copyright 2023 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "common.h"
#include "bcd_date.h"

#include "superblock.h"

#define	FDS_SUPERBLOCK_SIZE_BASE	58
#define	FDS_SUPERBLOCK_START		1
#define	FDS_FILECOUNT_START		2

static size_t fds_licensee_code_fprintf(FILE *file, fds_licensee_code_t code)
{
	const char *name;

	switch (code) {
	case FDS_LICENSEE_CODE_NINTENDO:
		name = "Nintendo";
		break;
	case FDS_LICENSEE_CODE_NULL:
		name = "NULL";
		break;
	default:
		name = UNKNOWN_STRING;
		break;
	}

	return fprintf(file, "%s", name);
}

static size_t fds_game_type_fprintf(FILE *file, fds_game_type_t type)
{
	const char *type_str;

	switch (type) {
	case FDS_GAME_TYPE_NORMAL:
		type_str = "Normal";
		break;
	case FDS_GAME_TYPE_EVENT:
		type_str = "Event";
		break;
	case FDS_GAME_TYPE_NETWORK:
		type_str = "Network";
		break;
	case FDS_GAME_TYPE_REDUCED:
		type_str = "Reduced";
		break;
	default:
		type_str = UNKNOWN_STRING;
		break;
	}

	return fprintf(file, "%s", type_str);
}

static size_t fds_disk_side_fprintf(FILE *file, fds_disk_side_t side)
{
	const char *side_str;

	switch (side) {
	case FDS_DISK_SIDE_A:
		side_str = "A";
		break;
	case FDS_DISK_SIDE_B:
		side_str = "B";
		break;
	default:
		side_str = UNKNOWN_STRING;
		break;
	}

	return fprintf(file, "%s", side_str);
}

static size_t fds_disk_type_fprintf(FILE *file, fds_disk_type_t type)
{
	const char *type_str;

	switch (type) {
	case FDS_DISK_TYPE_NORMAL:
		type_str = "Normal";
		break;
	case FDS_DISK_TYPE_FMC:
		type_str = "FMC";
		break;
	default:
		type_str = UNKNOWN_STRING;
		break;
	}

	return fprintf(file, "%s", type_str);
}

static size_t fds_disk_region_fprintf(FILE *file, fds_disk_region_t region)
{
	const char *region_str;

	switch (region) {
	case FDS_DISK_REGION_JAPAN:
		region_str = "Japan";
		break;
	case FDS_DISK_REGION_NULL:
		region_str = "NULL";
		break;
	default:
		region_str = UNKNOWN_STRING;
		break;
	}

	return fprintf(file, "%s", region_str);
}

static size_t fds_disk_color_fprintf(FILE *file, fds_disk_color_t color)
{
	const char *color_str;

	switch (color) {
	case FDS_DISK_COLOR_YELLOW:
		color_str = "Yellow";
		break;
	case FDS_DISK_COLOR_PROTO:
		color_str = "Proto";
		break;
	case FDS_DISK_COLOR_BLUE:
		color_str = "Blue";
		break;
	default:
		color_str = UNKNOWN_STRING;
		break;
	}

	return fprintf(file, "%s", color_str);
}

fds_superblock_t *fds_superblock_create(fds_superblock_t *superblock)
{
	superblock->start_marker = FDS_SUPERBLOCK_START;
	memcpy(superblock->security_string, FDS_SECURITY_STRING, sizeof (*superblock->security_string));
	superblock->licensee_code = FDS_LICENSEE_CODE_NULL;
	superblock->program_title[0] = '\0';
	superblock->program_title[1] = '\0';
	superblock->program_title[2] = '\0';
	superblock->game_type = FDS_GAME_TYPE_NORMAL;
	superblock->game_version = 0;
	superblock->disk_side_real = superblock->disk_side = FDS_DISK_SIDE_A;
	superblock->disk_number = 0;
	superblock->disk_type = FDS_DISK_TYPE_NORMAL;
	superblock->pad0 = 0;
	superblock->start_file_max = 0;
	superblock->pad1[0] = 0xFF;
	superblock->pad1[1] = 0xFF;
	superblock->pad1[2] = 0xFF;
	superblock->pad1[3] = 0xFF;
	superblock->pad1[4] = 0xFF;
	fds_bcd_date_create(&superblock->date_created);
	superblock->disk_region = FDS_DISK_REGION_NULL;
	superblock->unk0 = 0x61;
	superblock->unk1 = 0;
	superblock->unk2[0] = 0;
	superblock->unk2[1] = 2;
	superblock->unk3[0] = 0;
	superblock->unk3[1] = 0;
	superblock->unk3[2] = 0;
	superblock->unk3[3] = 0;
	superblock->unk3[4] = 0;
	fds_bcd_date_create(&superblock->date_modified);
	superblock->unk4 = 0;
	superblock->unk5 = 0x80;
	superblock->disk_writer = 0;
	superblock->unk6 = 0;
	superblock->disk_write_count = 0;
	superblock->disk_color = FDS_DISK_COLOR_YELLOW;
	superblock->disk_version = 0;
	superblock->crc0 = 0;

	superblock->file_count_marker = FDS_FILECOUNT_START;
	superblock->file_count = 0;
	superblock->crc1 = 0;

	return superblock;
}

int fds_superblock_read(fds_superblock_t *superblock, FILE *image, int cflag)
{
	superblock->start_marker = getc(image);
	fread(superblock->security_string, sizeof (*superblock->security_string), sizeof (superblock->security_string), image);
	superblock->licensee_code = getc(image);
	fread(superblock->program_title, sizeof (*superblock->program_title), sizeof (superblock->program_title), image);
	superblock->game_type = getc(image);
	superblock->game_version = getc(image);
	superblock->disk_side = getc(image);
	superblock->disk_number = getc(image);
	superblock->disk_type = getc(image);
	superblock->pad0 = getc(image);
	superblock->start_file_max = getc(image);
	fread(superblock->pad1, sizeof (*superblock->pad1), sizeof (superblock->pad1), image);
	fds_bcd_date_read(&superblock->date_created, image);
	superblock->disk_region = getc(image);
	superblock->unk0 = getc(image);
	superblock->unk1 = getc(image);
	fread(superblock->unk2, sizeof (*superblock->unk2), sizeof (superblock->unk2), image);
	fread(superblock->unk3, sizeof (*superblock->unk3), sizeof (superblock->unk3), image);
	fds_bcd_date_read(&superblock->date_modified, image);
	superblock->unk4 = getc(image);
	superblock->unk5 = getc(image);
	getw_m(superblock->disk_writer, image);
	superblock->unk6 = getc(image);
	superblock->disk_write_count = getc(image);
	superblock->disk_side_real = getc(image);
	superblock->disk_color = getc(image);
	superblock->disk_version = getc(image);

	if (cflag)
		getw_m(superblock->crc0, image);

	superblock->file_count_marker = getc(image);
	superblock->file_count = getc(image);

	if (cflag)
		getw_m(superblock->crc1, image);

	return feof(image) ? -1 : (FDS_SUPERBLOCK_SIZE_BASE + (cflag ? (2 * CRC_SIZE) : 0));
}

int fds_superblock_validate(const fds_superblock_t *superblock)
{
	if (superblock->start_marker != FDS_SUPERBLOCK_START)
		return 0;

	if (!strncmp(superblock->security_string, FDS_SECURITY_STRING, sizeof (superblock->security_string)))
		return 0;

	if (superblock->disk_side > FDS_DISK_SIDE_B || superblock->disk_side_real > FDS_DISK_SIDE_B)
		return 0;

	if (superblock->file_count_marker != FDS_FILECOUNT_START)
		return 0;

	return 1;
}

int fds_superblock_write(const fds_superblock_t *superblock, FILE *image, int cflag)
{
	putc(superblock->start_marker, image);
	fwrite(superblock->security_string, sizeof (*superblock->security_string), sizeof (superblock->security_string), image);
	putc(superblock->licensee_code, image);
	fwrite(superblock->program_title, sizeof (*superblock->program_title), sizeof (superblock->program_title), image);
	putc(superblock->game_type, image);
	putc(superblock->game_version, image);
	putc(superblock->disk_side, image);
	putc(superblock->disk_number, image);
	putc(superblock->disk_type, image);
	putc(superblock->pad0, image);
	putc(superblock->start_file_max, image);
	fwrite(superblock->pad1, sizeof (*superblock->pad1), sizeof (superblock->pad1), image);
	fds_bcd_date_write(&superblock->date_created, image);
	putc(superblock->disk_region, image);
	putc(superblock->unk0, image);
	putc(superblock->unk1, image);
	fwrite(superblock->unk2, sizeof (*superblock->unk2), sizeof (superblock->unk2), image);
	fwrite(superblock->unk3, sizeof (*superblock->unk3), sizeof (superblock->unk3), image);
	fds_bcd_date_write(&superblock->date_modified, image);
	putc(superblock->unk4, image);
	putc(superblock->unk5, image);
	putw_m(superblock->disk_writer, image);
	putc(superblock->unk6, image);
	putc(superblock->disk_write_count, image);
	putc(superblock->disk_side_real, image);
	putc(superblock->disk_color, image);
	putc(superblock->disk_version, image);

	if (cflag)
		putw_m(superblock->crc0, image);

	putc(superblock->file_count_marker, image);
	putc(superblock->file_count, image);

	if (cflag)
		putw_m(superblock->crc1, image);

	return ferror(image) ? -1 : (FDS_SUPERBLOCK_SIZE_BASE + (cflag ? (2 * CRC_SIZE) : 0));
}

size_t fds_superblock_fprintf(FILE *file, const fds_superblock_t *superblock)
{
	size_t size = 0;

	size += fprintf(file, "Security String: ");
	size += fwrite(superblock->security_string, sizeof (*superblock->security_string), sizeof (superblock->security_string), file);
	size += fprintf(file, "\n");

	size += fprintf(file, "Licensee Code: ");
	size += fds_licensee_code_fprintf(file, superblock->licensee_code);
	size += fprintf(file, "\n");

	size += fprintf(file, "Game Name: ");
	size += fwrite(superblock->program_title, sizeof (*superblock->program_title), sizeof (superblock->program_title), file);
	size += fprintf(file, "\n");

	size += fprintf(file, "Game Type: ");
	size += fds_game_type_fprintf(file, superblock->game_type);
	size += fprintf(file, "\n");

	size += fprintf(file, "Game Version: %d\n", superblock->game_version);

	size += fprintf(file, "Disk Side: ");
	size += fds_disk_side_fprintf(file, superblock->disk_side);
	size += fprintf(file, "\n");

	size += fprintf(file, "Disk Number: %d\n", superblock->disk_number);

	size += fprintf(file, "Disk Type: ");
	size += fds_disk_type_fprintf(file, superblock->disk_type);
	size += fprintf(file, "\n");

	size += fprintf(file, "Start File Max: $%x\n", superblock->start_file_max);

	size += fprintf(file, "Creation Date: ");
	size += fds_bcd_date_fprintf(file, &superblock->date_created);
	size += fprintf(file, "\n");

	size += fprintf(file, "Region: ");
	size += fds_disk_region_fprintf(file, superblock->disk_region);
	size += fprintf(file, "\n");

	size += fprintf(file, "Modification Date: ");
	size += fds_bcd_date_fprintf(file, &superblock->date_modified);
	size += fprintf(file, "\n");

	size += fprintf(file, "Disk Writer: $");
	fprintw_m(file, superblock->disk_writer);
	size += 4;
	size += fprintf(file, "\n");

	size += fprintf(file, "Disk Write Count: %d\n", superblock->disk_write_count);

	size += fprintf(file, "Disk Side Real: ");
	size += fds_disk_side_fprintf(file, superblock->disk_side_real);
	size += fprintf(file, "\n");

	size += fprintf(file, "Disk Color: ");
	size += fds_disk_color_fprintf(file, superblock->disk_color);
	size += fprintf(file, "\n");

	size += fprintf(file, "Disk Version: %d\n", superblock->disk_version);

	size += fprintf(file, "File Count: %d\n", superblock->file_count);

	return ferror(file) ? -1 : size;
}
