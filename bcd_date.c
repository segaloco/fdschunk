/*
 * bcd_date - Famicom Disk System Image BCD date handling routines
 *
 * Copyright 2023 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include <limits.h>

#include "bcd_date.h"

#define	FDS_BCD_DATE_SIZE	3

static unsigned char bcd_to_uchar(unsigned char bcd)
{
	unsigned char result = bcd & 0xF;

	result += (10 * ((bcd & 0xF0) >> (CHAR_BIT / 2)));

	return result;
}

static unsigned char fds_bcd_date_getyear(unsigned char year)
{
	switch (year) {
	case 61:
	case 62:
	case 63:
	case 64:
	case 65:
	case 66:
	case 67:
	case 68:
	case 69:
	case 70:
	case 71:
	case 72:
	case 73:
	case 74:
	case 75:
	case 76:
	case 77:
	case 78:
		return year + 25;
	case 1:
	case 2:
	case 3:
		return year + 88;
	default:
		return year;
	}
}

fds_bcd_date_t *fds_bcd_date_create(fds_bcd_date_t *date)
{
	date->year = 0;
	date->month = 0;
	date->day = 0;

	return date;
}

int fds_bcd_date_read(fds_bcd_date_t *date, FILE *file)
{
	date->year = getc(file);
	date->month = getc(file);
	date->day = getc(file);

	return feof(file) ? -1 : FDS_BCD_DATE_SIZE;
}

int fds_bcd_date_write(const fds_bcd_date_t *date, FILE *file)
{
	putc(date->year, file);
	putc(date->month, file);
	putc(date->day, file);

	return ferror(file) ? -1 : FDS_BCD_DATE_SIZE;
}

size_t fds_bcd_date_fprintf(FILE *file, const fds_bcd_date_t *date)
{
	size_t size;
	unsigned char year, month, day;

	year = bcd_to_uchar(date->year);
	month = bcd_to_uchar(date->month);
	day = bcd_to_uchar(date->day);

	return fprintf(file, "%u/%u/%u (yy/mm/dd)", fds_bcd_date_getyear(year), month, day);
}
