/*
 * file - Famicom Disk System Image file routines
 *
 * Copyright 2023 Matthew Gilmore
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

#include "file.h"

#define	FDS_FILEHEADER_SIZE_BASE	17
#define	FDS_FILEHEADER_START		3
#define	FDS_FILEPAYLOAD_START		4

static size_t fds_file_type_fprintf(FILE *file, fds_file_type_t type)
{
	const char *type_str;

	switch (type) {
	case FDS_FILE_TYPE_PRG:
		type_str = "PRG";
		break;
	case FDS_FILE_TYPE_CHR:
		type_str = "CHR";
		break;
	case FDS_FILE_TYPE_BG:
		type_str = "BG";
		break;
	default:
		type_str = UNKNOWN_STRING;
		break;
	}

	return fprintf(file, "%s", type_str);
}

fds_file_t *fds_file_create(fds_file_t *file)
{
	file->start_marker = FDS_FILEHEADER_START;
	file->file_number = 0;
	file->file_code = 0;
	file->file_name[0] = '\0';
	file->file_size = 0;
	file->file_type = FDS_FILE_TYPE_PRG;
	file->crc0 = 0;
	file->payload_marker = FDS_FILEPAYLOAD_START;
	file->file_data = NULL;
	file->crc1 = 0;

	return file;
}

int fds_file_read(fds_file_t *file, FILE *image, int cflag)
{
	file->start_marker = getc(image);
	file->file_number = getc(image);
	file->file_code = getc(image);
	fread(file->file_name, sizeof (*file->file_name), sizeof (file->file_name), image);
	getw_m(file->file_dest, image);
	getw_m(file->file_size, image);
	file->file_type = getc(image);

	if (cflag)
		getw_m(file->crc0, image);

	file->payload_marker = getc(image);

	file->file_data = malloc(file->file_size);
	fread(file->file_data, sizeof (*file->file_data), file->file_size, image);

	if (cflag)
		getw_m(file->crc1, image);

	return feof(image) ? -1 : (FDS_FILEHEADER_SIZE_BASE + (cflag ? (2 * CRC_SIZE) : 0) + file->file_size);
}

int fds_file_validate(const fds_file_t *file)
{
	if (file->start_marker != FDS_FILEHEADER_START)
		return 0;

	if (file->payload_marker != FDS_FILEPAYLOAD_START)
		return 0;

	return 1;
}

int fds_file_write(const fds_file_t *file, FILE *image, int cflag)
{
	putc(file->start_marker, image);
	putc(file->file_number, image);
	putc(file->file_code, image);
	fwrite(file->file_name, sizeof (*file->file_name), sizeof (file->file_name), image);
	putw_m(file->file_dest, image);
	putw_m(file->file_size, image);
	putc(file->file_type, image);

	if (cflag)
		putw_m(file->crc0, image);

	putc(file->payload_marker, image);

	fwrite(file->file_data, sizeof (*file->file_data), file->file_size, image);

	if (cflag)
		putw_m(file->crc1, image);

	return ferror(image) ? -1 : (FDS_FILEHEADER_SIZE_BASE + (cflag ? (2 * CRC_SIZE) : 0) + file->file_size);
}

size_t fds_file_fprintf(FILE *out, const fds_file_t *file)
{
	size_t size = 0;

	size += fprintf(out, "\n");
	size += fprintf(out, "File Number: %d\n", file->file_number);
	size += fprintf(out, "File Code: $%.2x\n", file->file_code);

	size += fprintf(out, "File Name: ");
	size += fwrite(file->file_name, sizeof (*file->file_name), sizeof (file->file_name), out);
	size += fprintf(out, "\n");

	size += fprintf(out, "File Destination Address: $");
	fprintw_m(out, file->file_dest);
	size += 4;
	size += fprintf(out, "\n");

	size += fprintf(out, "File Size: $");
	fprintw_m(out, file->file_size);
	size += 4;
	size += fprintf(out, "\n");

	size += fprintf(out, "File Type: ");
	size += fds_file_type_fprintf(out, file->file_type);
	size += fprintf(out, "\n");

	return ferror(out) ? -1 : size;
}

void fds_file_free(fds_file_t *file)
{
	if (file->file_data != NULL) {
		free(file->file_data);
		file->file_data = NULL;
	}

	file->file_size = 0;
}
